const Odoo = require('odoo-xmlrpc');
// var config = require('config');

const flectra = new Odoo({
    "url":"127.0.0.1",
    "port":7073,
    "db":"user",
    "username":"admin",
    "password":"admin"

})
//methods
const CREATE = 'create'
const WRITE = 'write'
const SEARCH_READ = 'search_read'
const UNLINK = 'unlink'
const CONVERT_OPPORTUNITY = 'convert_opportunity'
const CREATE_OR_REPLACE = 'create_or_replace'
//models
const CRM_LEAD = 'crm.lead'
const CRM_LEAD_TAG = 'crm.lead.tag'
const RES_PARTNER = 'res.partner'
const RES_PARTNER_CATEGORY = 'res.partner.category'
const RES_PARTNER_TITLE = 'res.partner.title'
const RES_COUNTRY = 'res.country'
const RES_COUNTRY_STATE = 'res.country.state'
const IR_FILTERS = 'ir.filters'
const PRODUC_TEMPLATE = 'product.template'

/* 
colors {
    1: red
    3: yellow
    4: blue
    10: green
} 
*/

var flectraConnect = () => {
    return new Promise(( resolve, reject) => {
        flectra.connect(async err => {
            if (err) {
                console.log('Connection to Flectra error: ', err)
                reject(err)
            }
            console.log('Connected to Flectra server.')
            resolve(flectra);
        })
    })
}

var execute_kw = async (model, method, params) => {
    return new Promise(async ( resolve, reject) => {
        flectraConnect().then( flectra => {
            flectra.execute_kw(model, method, params, async (err, value) => {
                if (err) {
                    console.log(method, model, 'params: ', params, ' error: ', err)
                    reject(err)
                }
                console.log(method, model );   
                console.log(value);
                
                resolve(value)
            })
        }).catch( err => {
            reject(err)
        })
    })
}



var getElement = async (model, filter, fields, offset, limit) => {

    filter = filter || 0
    fields = fields || 0
    limit = limit || 0
    offset = offset || 0
    var inParams = [];
    inParams.push(filter);
    inParams.push(fields);
    inParams.push(offset);
    inParams.push(limit);
    var params = [];
    params.push(inParams);
    let result = await execute_kw(model, SEARCH_READ, params).catch( err => {
        throw new Error(err)
    })
    if (model === RES_PARTNER_TITLE) {
        console.log(' title result:', JSON.stringify(params), JSON.stringify(result));
        
    }
    if (fields) {
        let array = []
        for(let elementIndex in result){
            let element = result[elementIndex]
            let newElement = {}
            for(let fieldIndex in fields) {
                let field = fields[fieldIndex]
                newElement[field] = element[field]
            }
            array.push(newElement)
        }
        result = array
    }
    result = (limit===1)? result[0] : result
    return result
}

var existElemnt = async (model, filter) => {
    return await getElement(model, filter, 0, 0, 1)
}

var createElement = async (context, model, filter, element) => {
    if (!flectra || !model || !element) {
        return
    }

    context = context || {}
    let elementId = await existElemnt(model, filter)
    console.log('elemenId:', elementId);
    
    if (!elementId || elementId.length === 0) {
        var inParams = [];
        inParams.push(element);
        inParams.push(context);
        var params = [];
        params.push(inParams);
        return  await execute_kw(model, CREATE, params).catch( err => {
            throw new Error(err)
        })
    } else  
        return  {elementId: elementId, allReadyExist: true}
}

var updateElement = async (model, element) => {
    if (!flectra || !model || !element || !element.id) {
        return
    }

    var inParams = [];
    inParams.push([element.id]);
    inParams.push(element)
    var params = [];
    params.push(inParams);

    return await execute_kw(model, WRITE, params)
}

var deleteElement = async (model, id) => {
    var inParams = [];
    inParams.push([id]); //id to delete
    var params = [];
    params.push(inParams);

    return await execute_kw(model, UNLINK, params)
}

var getLeads = async filter => {
    return await getElement(CRM_LEAD, filter)
}

var getLead = async filter => {
    return await getElement(CRM_LEAD, filter, 0, 0, 1)
}

var getLeadTags = async filter => {
    return await getElement(CRM_LEAD_TAG, filter, 0, 0, 0)
}

var getLeadTagsIds = async filter => {
    return await getElement(CRM_LEAD_TAG, filter, 0, 0, 0)
}

var getLeadTagId = async filter => {
    let result = await getElement(CRM_LEAD_TAG, filter, 0, 0, 1)
    return result
}

// create lead.tag to flectra 
var createLeadTag = async tag => {
    let filter = [
        ['name', '=', tag.name]
    ]
    if (tag.id) filter = [
        ['id', '=', tag.id]
    ]
    return await createElement('', CRM_LEAD_TAG, filter, tag).catch( err => {
        throw new Error(err)
    })
}

var updateLeadTag = async tag => {
    return await updateElement(CRM_LEAD_TAG, tag).catch( err => {
        throw new Error(err)
    })
}

var deleteLeadTag = async tag => {
    let filter = [
        ['name', '=', tag.name]
    ]
    if (tag.id) filter = [
        ['id', '=', tag.id]
    ]
    getLeadTagsIds(filter, (err, tagId) => {
        if (tagId && tagId.length > 0)
            deleteElement(CRM_LEAD_TAG, tagId[0].id)
    })
}

// create lead type:opportunity to flectra 
var createLead = async lead => {
    
    if (!lead) {
        return
    }

    let leadFilter = [
        ['name', 'ilike', lead.name]
    ]

    let context = (lead.stage_id > 1)? {default_type: 'opportunity'} : ''
        
    let {elementId, allReadyExist} = await createElement(context, CRM_LEAD, leadFilter, lead)

    if (allReadyExist) {
        await updateLead(lead)
    }  

    return elementId
}

var updateLead = async lead => {
    if (!lead || !lead.id) {
        return
    }

    let leadFilter = [['name','ilike',lead.name]]

    let leadResult = await getLead(leadFilter)
    console.log('leadresult:', leadResult);
        
    if (!leadResult) {
        return await createLead(lead)
    } else {
        let updatedElement = await updateElement(CRM_LEAD, lead)
        if (lead.stage_id > 1 && leadResult.type === 'lead') {
            await convertLeadToOpportunity(lead)
        } 
        return updatedElement
    }
}

var createContactTags = async tag => {
    if (!tag) {
        console.log('no tag recived');
        return
    }
    let filter = [['name','ilike',tag.name]]
    return await createElement('', RES_PARTNER_CATEGORY, filter, tag)
}

var getContactTag = async name => {
    let filter = [['name','ilike',name]]
    return await getElement(RES_PARTNER_CATEGORY, filter, ['id'], 0, 1).catch( err => {
        throw new Error(err)
    })
}

var addTagsToContact = (contact, tags) => {

}

var getCountry = async name => {
    let filter = [['code','ilike',name]]
    return getElement(RES_COUNTRY, filter, ['id'], 0, 1)
}

var getState = async (name, countryId) => {
    let filter = ['&',['country_id','ilike',countryId],['code','ilike',name]]
    return await getElement(RES_COUNTRY_STATE, filter, ['id'], 0, 1)
}

var getTitle = async name => {
    let filter = [['shortcut','ilike',name]]
    
    return await getElement(RES_PARTNER_TITLE, filter, ['id'], 0, 1)
}

var createOpportunity = async opportunity => {
    opportunity.default_type = 'opportunity'
    return await createLead(opportunity)
}

var convertLeadContactToClient = async element => {
    if (!element) {
        return
    }
    
    return await execute_kw(CRM_LEAD, 'handle_partner_assignation', [[element.id], {action: 'create' }])
}

// change state lead
var changeLeadStage = async (opportunity, new_stage_id) => {
    opportunity.stage_id = new_stage_id
    return await updateElement(CRM_LEAD, opportunity)
}

// convert lead to opportunity
var convertLeadToOpportunity = async opportunity => {
    // console.log('convertLeadToOpportunity', opportunity);
    return await execute_kw(CRM_LEAD, CONVERT_OPPORTUNITY, [[opportunity.id], { partner_id: opportunity.partner_id }])
}

// add new tags
var addTagsToLead = async (lead, tag_names) => {
    let filter = [
        ['name', 'in', tag_names]
    ]
    // console.log('addtagsLuead lead', lead);

    let tag_ids = await getElement(CRM_LEAD_TAG, filter, ['id'], 0, 0)
    let tags = lead.tag_ids || []
    if (tag_ids && tag_ids.length) {
        for (let tag of tag_ids) {
            tags.push(tag.id)
        }
        lead.tag_ids = [
            [6, 0, tags]
        ]
        // console.log('getelement tag_ids:', tag_ids);
        return await updateElement(CRM_LEAD, lead)
    }
}

// remove some tags 
var removeTagsToLead = async (lead, tag_names) => {
    let filter = [
        ['name', 'in', tag_names]
    ]
    let tag_ids = await getElement(CRM_LEAD_TAG, filter, ['id'], 0, 0)
    lead.tag_ids = [[3, tag_ids]] // [[6, 0, tag_ids]]
    return await updateElement(CRM_LEAD, lead)
}

// create contact to flectra 
var createContact = async contact => {
    if (!contact || !contact.email) {
        return
    }

    contact.id = await execute_kw(RES_PARTNER, 'find_or_create', [[contact.email]])
 
    return await updateElement(RES_PARTNER, contact)
}

var updateContact = async contact => {
    if (!contact || !contact.email || !contact.id) {
        return
    }
    return await updateElement(RES_PARTNER, contact)
}

// get contacts from flectra
var getContacts = async filter => {
    return await getElement(RES_PARTNER, filter, 0, 0, 0)
}

// get contact from flectra
var getContact = async filter => {
    return await getElement(RES_PARTNER, filter, ['id'], 0, 1)
}

// create ir.filters to flectra create_or_replace
var createFilter = async element => {
    if (!flectra || !element || !element.name) {
       return
    }

    let filter = [['name','ilike',element.name]]

    element.sort || '[]'
    element.context || '{}'
    element.domain || '[]'
    element.model_id || 'crm.lead'

    var inParams = [];
    inParams.push(element);
    var params = [];
    params.push(inParams);

    return await createElement('', IR_FILTERS, filter, params).catch( err => {
        throw new Error(err)
    })
}

// getFilters 
var getFilters = async filter => {
    return await getElement(IR_FILTERS, filter, 0, 0, 0)
}

// getFilter 
var getFilter = async filter => {
    return await getElement(IR_FILTERS, filter, 0, 0, 1)
}

var createProduct = async product => {
    if (!product) {
        return
    }

    let filter = [
        ['name', '=', product.name]
    ]

    return await createElement('', PRODUC_TEMPLATE, filter, product)
}

var updateProduct = async product => {
    if (!product || !product.id) {
        return
    }

    return await updateElement(PRODUC_TEMPLATE, product)
}



///////////////////////////////////////////
var getProductAttributeFromFlectra = (filters, callback) => {
    if (!filters) {
        filters = 0 // se pone en cero si no quiero filtrar
    }

    var inParams = [];
    inParams.push(filters); // ([['name', '=', 'crusero']]) filter by field value, incase of list ([['name', '=', ['crusero'] ]])
    inParams.push(0); // (['field_name','other_field']) filter by fields, retunring only this fields
    inParams.push(0); //offset
    inParams.push(0); //limit
    var params = [];
    params.push(inParams);
    flectra.execute_kw('product.attribute', 'search_read', params, (err, value) => {
        if (err) {
            console.log(err);
        }
        if (value) {
            console.log('search_read product.attribute result: ', value)
            callback(err, value)
        }
    });
}

var getProductAttributeValueFromFlectra = (filters, callback) => {
    if (!filters) {
        filters = 0 // se pone en cero si no quiero filtrar
    }

    var inParams = [];
    inParams.push(filters); // ([['name', '=', 'crusero']]) filter by field value, incase of list ([['name', '=', ['crusero'] ]])
    inParams.push(0); // (['field_name','other_field']) filter by fields, retunring only this fields
    inParams.push(0); //offset
    inParams.push(0); //limit
    var params = [];
    params.push(inParams);
    flectra.execute_kw('product.attribute.value', 'search_read', params, function (err, value) {
        if (err) {
            console.log(err);
        }
        if (value) {
            console.log('search_read product.attribute.value result: ', value)
            callback(err, value)
        }
    });
}

var getProductsFromFlectra = (filters, callback) => {
    if (!filters) {
        filters = 0 // se pone en cero si no quiero filtrar
    }

    var inParams = [];
    inParams.push(filters); // ([['name', '=', 'crusero']]) filter by field value, incase of list ([['name', '=', ['crusero'] ]])
    // inParams.push((['name','price','standard_price','list_price','sale_ok','description','product_variant_ids'])); // (['field_name','other_field']) filter by fields, retunring only this fields
    inParams.push(0);
    inParams.push(0); //offset
    inParams.push(0); //limit
    var params = [];
    params.push(inParams);
    flectra.execute_kw('product.product', 'search_read', params, (err, value) => {
        if (err) {
            console.log(err);
        }
        if (value) {
            console.log('search_read product.product result: ', value)
            callback(err, value)
        }
    });
}

var createProductAttribute = (attribute, callback) => {
    let filter = [
        ['name', '=', attribute.name]
    ]
    getProductAttributeFromFlectra(filter, (existAttribute) => {
        console.log('existAttribute:', existAttribute);
        if (!existAttribute || existAttribute.length === 0) {
            var inParams = [];
            inParams.push({
                'name': attribute.name
            })
            var params = [];
            params.push(inParams);
            flectra.execute_kw('product.attribute', 'create', params, (err, value) => {
                if (err) {
                    console.error('create product.attribute error: ', err);
                } else {
                    console.log('create product.attribute result: ', value)
                    createProductAttributeValue(attribute, (attributeValue) => {})
                }
            });
        } else {
            createProductAttributeValue(attribute, (attributeValue) => {})
        }
    })
}

var createProductAttributeValue = (attribute, callback) => {
    let filter = [
        ['name', '=', attribute.value]
    ]
    getProductAttributeValueFromFlectra(filter, (existAttributeValue) => {
        console.log('existAttributeValue:', existAttributeValue);
        if (!existAttributeValue || existAttributeValue.length === 0) {
            let attributeFilter = [
                ['name', '=', attribute.name]
            ]
            getProductAttributeFromFlectra(attributeflectra, filter, (attributeList) => {
                var inParams = [];
                inParams.push([{
                    'attribute_id': 1
                }])
                inParams.push([{
                    'name': 'FFNew Partner'
                }])

                var params = [];
                params.push(inParams);
                console.log('params:', JSON.stringify(params));

                flectra.execute_kw('product.attribute.value', 'create', inParams, (err, value) => {
                    if (err) {
                        console.error('create product.attribute.value error: ', err);
                    } else {
                        console.log('create product.attribute.value result: ', value)
                        callback(err, value)
                    }
                });
            })
        }
    })
}

module.exports = {
    flectraConnect: flectraConnect,
    createContact: createContact,
    createProduct: createProduct,
    createLead: createLead,
    createOpportunity: createOpportunity,
    createLeadTag: createLeadTag,
    createProductAttribute: createProductAttribute,
    createProductAttributeValue: createProductAttributeValue,
    createFilter: createFilter,
    createContactTags: createContactTags,
    updateLead: updateLead,
    updateProduct: updateProduct,
    updateContact: updateContact,
    deleteLeadTag: deleteLeadTag,
    getLeads: getLeads,
    getLead: getLead,
    getLeadTags: getLeadTags,
    getLeadTagId: getLeadTagId,
    getLeadTagsIds: getLeadTagsIds,
    getCountry: getCountry,
    getContact: getContact,
    getContactTag: getContactTag,
    getState: getState,
    getTitle: getTitle,
    getContacts: getContacts,
    getFilters: getFilters,
    getFilter: getFilter,
    addTagsToLead: addTagsToLead,
    removeTagsToLead: removeTagsToLead,
    changeLeadStage: changeLeadStage,
    convertLeadToOpportunity: convertLeadToOpportunity
}