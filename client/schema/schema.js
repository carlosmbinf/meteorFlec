import SimpleSchema from 'simpl-schema';

SimpleSchema.extendOptions(['autoform']);

// Books = new Mongo.Collection("books");
UserProfile = new SimpleSchema({
  name: {
    type: String,
    label: "Name"
  },
  family: {
    type: String,
    label: "Family"
  },
  address: {
    type: String,
    label: "Address",
    optional: true,
    max: 1000
  },
  workAddress: {
    type: String,
    label: "WorkAddress",
    optional: true,
    max: 1000
  },
  phoneNumber: {
    type: Number,
    label: "Phone Number",
    optional: true
  },
  mobileNumber: {
    type: Number,
    label: "Phone Number"
  },
  birthday: {
    type: Date,
    optional: true
  },
  gender: {
    type: String,
    allowedValues: ['Male', 'Female'],
    optional: true
  },
  description: {
    type: String,
    label: "Description",
    optional: true,
    max: 10000
  }

});
User = new SimpleSchema({
  username: {
    type: String,
    regEx: /^[a-z0-9A-Z_]{3,15}$/
  },
  emails: {
    type: String,
    // this must be optional if you also use other login services like facebook,
    // but if you use only accounts-password, then it can be required
    optional: true,
    
  },
  createdAt: {
    type: Date,
    optional: true
  },
  profile: {
    type: UserProfile,
    optional: true
  },
  //    // Add `roles` to your schema if you use the meteor-roles package.
  //    // Option 1: Object type
  //    // If you specify that type as Object, you must also specify the
  //    // `Roles.GLOBAL_GROUP` group whenever you add a user to a role.
  //    // Example:
  //    // Roles.addUsersToRoles(userId, [admin], Roles.GLOBAL_GROUP);
  //    // You can't mix and match adding with and without a group since
  //    // you will fail validation in some cases.
    //  
    








    
  //    // Option 2: [String] type
  //    // If you are sure you will never need to use role groups, then
  //    // you can specify [String] as the type
   
});
contact = new SimpleSchema({
  name: {
    type: String,
    label:"Name: ",
    
  },
  phone:{
    type: Number,
    label:"Phone Number ",
    regEx:/^[0-9]+$/

  },
  city:{
    type: String,
    label:"City: ",
    
  },
  
  id:{
    type: Number,
    label:"Number ID: ",
    
  },
  
  mobile:{
    type: Number,
    label:"Phone Number: ",
    optional: true
  },
  zip: {
    type: Number,
    label:"Code ZIP: ",
    
  },
  email:{
    type: String,
    label:"Email: ",
    
  },
  
  street: {
    type: String,
    label:"Street ",
    
  },
  
  
});
